<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Lessson 00: Welcome and introduction - Introduction to Programming (Moosadee.com)</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="web-assets/style/style.css">
    <link rel="icon" type="image/png" href="web-assets/favicon.png">
  </head>
  <body>
    
    <div class="container-fluid">
      <header>
        <div class="home-link"><a href="http://www.moosadee.com/"><img src="web-assets/graphics/home.png" title="Go back to Moosadee.com"></a></div>
        <div class="course-info">
          
          <p class="title">Introduction to Computer Science and Programming</p>
          <p class="author">Course created by Rachel Wil Sha Singh</p>
        </div> <!-- <div class="course-info"> -->
      </header>
      
      
      <div class="row main-view">
        <div class="col-md-12">
          &nbsp;
        </div>
        
        <nav class="col-md-3 col-sm-12 col-xs-12">
            <h3>Lesson 00: Welcome and introduction </h3>
            
            <p><a href="index.html">&lt;&lt; Back to the course frontpage</a></p>
            <ol>
              <li><a href="#part1">Welcome!</a></li>
              <li><a href="#partY">Questions and reflections</a></li>
            </ol>

      </nav>
      
      <main class="col-md-9 col-sm-12 col-xs-12">
        <div class="in-text-image float-right">
          <img src="lesson-assets/lesson00-writing_code.png" title="A drawing of Rachel writing code on a laptop, thinking 'uhhh...' and looking confused.">
        </div>
       
        <h2><a name="part1" href="#part1">📌</a> Welcome!</h2>
        <p>
          It feels weird to start a collection of notes (or a ``textbook'')
        without some sort of welcome, though at the same time I know that
        people are probably <em>not</em> going to read the introduction.
        <span class="footnote">Unless I put some cute art and interesting footnotes, maybe</span>
        <br><br>
        I think that I will welcome you to my notes by addressing anxiety. 
        </p>
        
        <h3>Belonging</h3>
        
        <p>
          Unfortunately there is a lot of bias in STEM fields and over
          decades there has been a narrative that computer science is <em>for</em>
          a certain type of person - antisocial, nerdy, people who started coding
          when they were 10 years old.
          <br><br>
          Because of this, a lot of people who don't fit this description can
          be hesitant to get into computers or programming because they don't
          see people like themselves in media portrayals. Or perhaps previous
          professors or peers have acted like you're not a ``real programmer''
          if you didn't start programming as a child.
        </p>
        
        <p style="text-align:center; font-weight: bold;">
          If you want to learn about coding, then you belong here.
          <br>
          There are no prerequisites.
        </p>
        
        <div class="clear"></div>
        
        <div class="in-text-image float-right">
          <img src="lesson-assets/lesson00-gatekeeping_baby.png" title="A drawing of a baby holding a computer mouse and floppy disk. The baby looks angry and is saying: I can't even walk yet but I know how to code, unlike YOU posers!">
          <p><span class="citation">Don't be like Gatekeeper Baby. You can begin coding at any age!</span></p>
        </div>
        
        <p>
        I will say from my own experience, I know developers who fell in
        love with programming by accident as an adult after having to take
        a computer class for a <em>different degree</em>. I know developers who
        are into all sorts of sports, or into photography, or into fashion.
        There is no specific ``type'' of programmer. You can be any religion,
        any gender, any color, from any country, and be a programmer.
        </p>
        
        <hr>
        <h3>Challenge</h3>
        
        <p>
           Programming can be hard sometimes. There are many aspects of learning
          to write software (or websites, apps, games, etc.) and you <strong>will</strong>
          get better at it with practice and with time. But I completely understand
          the feeling of hitting your head against a wall wondering <em>why won't this work?!</em>
          and even wondering <em>am I cut out for this?!</em> - Yes, you are.
          <br><br>
          I will tell you right now, I <em>have</em> cried over programming assignments,
          over work, over software. I have taken my laptop with me to a family
          holiday celebration because I <em>couldn't figure out this program</em>
          and I <em>had to get it done!!</em>
          <br><br>
          All developers struggle. Software is a really unique field. It's very
          intangible, and there are lots of programming languages, and all sorts
          of tools, and various techniques. Nobody knows everything, and there's
          always more to learn.
        </p>

        <div class="clear"></div>
        
        <div class="in-text-image float-right">
          <img src="lesson-assets/lesson00-justwork.png" title="A drawing of Rachel crying at their laptop saying: Why don't you just work?! The laptop is also drawn with a crying face.">
        </div>
        
        <p style="text-align:center;">
          Just because something is <em>hard</em> doesn't mean that it's <em>impossible</em>.
        </p>
        
        <p>
          It's completely natural to hit roadblocks. To have to step away from
          your program and come back to it later with a clear head. It's natural
          to be confused. It's natural to <em>not know</em>.
          <br><br>
          But some skills you will learn to make this process smoother are
          how to plan out your programs, test and verify your programs,
          how to phrase what you don't know as a question, how to ask for help.
          These are all skills that you will build up over time.
          Even if it feels like you're not making progress, I promise that you are,
          and hopefully at the end of our class you can look back to the start of it
          and realize how much you've learned and grown.
        </p>
        
        <hr>
        <h3>Learning</h3>
        
        <p>
          First and foremost, I am here to help you <strong>learn</strong>.
          <br><br>
          My teaching style is influenced on all my experiences throughout
          my learning career, my software engineer career, and my teaching career.
          I have personally met teachers who have tried to <em>scare me away</em>
          from computers, I've had teachers who really encouraged me, 
          I've had teachers who barely cared, I've had teachers who made
          class really fun and engaging.
          <br><br>
          I've worked professionally in software and web development, and
          independently making apps and video games. I know what it's like
          to apply for jobs and work with teams of people and experience
          a software's development throughout its lifecycle.
        </p>
                
        <p>
          And as a teacher I'm always trying to improve my classes -
          making the learning resources easily available and accessible,
          making assignments help build up your knowledge of the topics,
          trying to give feedback to help you design and write good programs.
          I am not here to trick you with silly questions
          or decide whether you're a ``real programmer'' or not;
          I am here to help guide you to learn about programming,
          learn about design, learn about testing, and learn how to teach yourself.
        </p>
        
        <div style="text-align:center;">
          <img src="lesson-assets/lesson00-buildingblocks.png" title="A drawing of Rachel by a series of building blocks numbered 1, 2, and 3. Rachel is saying: Like this!" style="width:300px;">
        </div>
           
        <div class="clear"></div>
        
        <hr><h2><a name="partY" href="#partY">📌</a> Questions and reflections </h2>
        <ul>
          <li>What is your learning style? Do you like reading? Watching videos? Practicing hands-on? What does and does not work for you?</li>
          <li>Keep in mind that programming is a puzzle - it's all about problem solving. Approach it that way, as a fun game. There will be challenges and frustrations, but you can overcome it!</li>
        </ul>
        
        <div class="clear"></div>
        
        
<!--
        <div class="in-text-image float-right">
          <img src="lesson-assets/" title="">
          <p><span class="citation">From <a href="">Wikimedia Commons / Mrjohncummings, CC BY-SA 2.0</a></span></p>
        </div>
-->
        
      </main>
      
      <footer>
        <p>Course by Rachel Wil Sha Singh (aka "Moose")</p>
        <p>Last updated <span id="update-date"></span></p>
        <p>Homepage: <a href="http://www.moosadee.com/">http://www.moosadee.com/</a></p>
        <p>Email: racheljmorris at gmail dot com</p>
        <p>Course repository: <a href="https://gitlab.com/moosadee/course-introduction-to-programming">on GitLab</a></p>
      </footer>
      </div> <!-- row -->
      
    </div> <!-- container-fluid -->
    
    <script>
      document.getElementById( "update-date" ).innerHTML = document.lastModified;
    </script>
    
  </body>
</html>
